# Skeleton for most of IDI MSs #

If you have any suggestions how to update and make it better, just have ideas to share do not hesitate to do it:) All ideas welcome.

### What is this repository for? ###

* Skeleton is an helper for all Micro Services
* 0.1

### How do I get set up? ###

* Install Composer
* Add "idiapi/skeleton" into require list

```json
"repositories": [
	{
		"type": "vcs",
		"url": "https://bitbucket.org/idi-dev-team/skeleton.git"
	}
],
"require": {       
	"idiapi/skeleton": "dev-master"
}
```

* Then in MS app/app.php 

		require 'vendor/idiapi/skeleton/src/loader.php';

* After installation in case of Skeleton update, use 'composer update' to get latest version

### Basic Structure ###

 - `vendor/` all 3rd party libs goes here (auto install via composer)
 - `app/libs/` all custom libs required in all MSs
 - `src/controller.php` all MSs controllers are extended from this controller 
 - `src/bootstrap.php` loads everything for unit testing to be called from each MS
 - `src/loader.php` APP loader file

### Contribution guidelines ###

* Writing code
* Writing tests
* Code review
* Sleep:)

### Changelog ###

* 0.1 Initial setup

### Who do I talk to? ###

* Brindau Andrei (andrei@web-foundry.co.uk)
* Web-Foundry (http://www.web-foundry.co.uk)