<?php

define('PROJECT_ROOT', realpath(__DIR__ . '/../../../..'));
chdir(PROJECT_ROOT);

// Fix notices because we are not using Slim from Browser context
$_SERVER['REQUEST_URI'] = $_SERVER['REMOTE_ADDR'] = $_SERVER['SERVER_NAME'] = $_SERVER['REQUEST_METHOD'] = '';

/* Load Composer Autoload */
require_once PROJECT_ROOT . '/vendor/autoload.php';

/* Load Controllers */
require_once PROJECT_ROOT . "/vendor/idiapi/skeleton/src/config.php";
require_once PROJECT_ROOT . "/app/config.php";
require_once PROJECT_ROOT . "/vendor/idiapi/skeleton/src/authentication.php";
require_once PROJECT_ROOT . "/vendor/idiapi/skeleton/src/dbManager.php";
require_once PROJECT_ROOT . "/vendor/idiapi/skeleton/src/application.php";
require_once PROJECT_ROOT . "/vendor/idiapi/skeleton/src/model.php";
require_once PROJECT_ROOT . "/vendor/idiapi/skeleton/src/controller.php";
foreach (glob(PROJECT_ROOT . "/app/controllers/*.php") as $controller) {
    require_once $controller;
}

/* Load General Libs */
require_once "libs/auth/apiAuth.php";
require_once "libs/crudList/autoload.php";
require_once "libs/hooks/hooks.php";

/* Load cache class and cacheMiddleware */
require_once "libs/cache/cache.php";
require_once "libs/cache/cacheMiddleware.php";

/* Load Models */
foreach (glob(PROJECT_ROOT . "/app/models/*.php") as $model) {
    require_once $model;
}

/* Load Libs */
ApiConfig::loadLibs();

$logger = new \Flynsarmy\SlimMonolog\Log\MonologWriter(
    array(
        'name' => 'idi-api-'.ApiConfig::get('msName'),
        'handlers' => array(
            new \Monolog\Handler\StreamHandler('./logs/'.date('Y-m-d').'.log'),
        ),
));

/* Add containers to Slim */
$app = new \Slim\Slim([
    'log.writer' => $logger,
    'log.enabled' => true,
]);

// Hooks
$app->container->singleton('hooks', function () {
    return new \api\Hooks();
});

$app->container->singleton('auth', function () {
    return new \api\Auth\Authentication();
});


$clientArgs = array_filter(
    $argv,
    function($in) {return 0 === strpos($in, "client_id=");}
);
$app->auth->client = (object)['id' => substr(current($clientArgs), 10)];
if (!$app->auth->client->id) {
    throw new Exception ("client_id must be set");
} 

// Connect to Default Database as we don't have token yet
$dbManager = new \dbManager(\ApiConfig::get('msName'));
if (!$dbManager->connect(\ApiConfig::get('dbHandler'))) {
    $this->throwError('Connection with database interrupted');
}

$app->hooks->startConsumer();