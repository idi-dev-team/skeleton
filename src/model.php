<?php

class Model extends Application 
{
	public function __construct($app = null) 
	{
		parent::__construct($app);
	}
	
	public function paginate($data, $start = 0, $limit = 20) 
	{
		$count = count($data);
		$data = array_values($data);
		$data = array_slice($data, $start, $limit);
		return array(
			'data' => $data,
			'count' => $count,
			'start' => $start,
			'limit' => $limit,
		);
	}
	
	public function arrayUnion($array1, $array2, $key1, $key2 = null, $appendFields = array(), $sortField = null, $ascending = true) 
	{
		if (!$key2) $key2 = $key1;
		$return = array();
		
		foreach ($array1 as $object1) {
			$exists = false;
			$object1 = (object)$object1;
			
			foreach ($array2 as $object2) {
				$object2 = (object)$object2;
				
				if (isset($object2->{$key2}) && isset($object1->{$key1}) && $object1->{$key1} == $object2->{$key2}) {
					$exists = true;
                    foreach ($appendFields as $field) {
                        if (isset($object2->{$field})) {
                            $object1->{$field} = $object2->{$field};
                        }
                    }
				}
			}
			
			if ($exists) {
				$return[] = $object1;
			}
		}
        
		if ($sortField) {
			usort($return, function($a, $b) use ($ascending, $sortField) {
				if ($ascending) {
					return $a->{$sortField} >= $b->{$sortField}; 
				} else {
					return $a->{$sortField} < $b->{$sortField}; 
				}
			});
		}

		return $return;
	}
	
	public function doSearch($entity, $filters, $start = false, $limit = false, $sort = false, $dir = 'ASC')
	{
		$query = C::table($entity);
		
		foreach ($filters as $filter) {
			$fields = explode(',', $filter['field']);
			
			switch ($filter['operator']) {
				case "%":
					$whereRaw = $bindings = array();
					foreach ($fields as $field) {
						$whereRaw[] = sprintf('%s LIKE ?', $field);
						$bindings[] = '%' . $filter['value'] . '%';
					}
					$query->whereRaw("(" . implode(' OR ', $whereRaw) . ")", $bindings);
					break;
				default:
					$query->where($fields[0], $filter['operator'], $filter['value']);
					break;
			}
			
		}

        if ($sort) {
            $query->orderBy($sort, $dir);
        }
		
		if ($start && $limit) {
			$query->limit((int)$limit . ', ' . (int)$start);
		}
		$data = $query->get();
		
		if ($start && $limit) {
			return array(
				'data'  => $data,
				'count' => $query->count(),
				'start' => $start,
				'limit' => $limit,
			);
		} else {
			return $data;
		}
	}
    
    public function updateTableStructure($structure)
    {
        foreach ($structure as $table => $fields) 
        {
            if (!C::schema()->hasTable($table)) {
                C::schema()->create($table, function ($table) use ($fields) {
                   foreach ($fields as $column => $type) {
                       if ($type == 'increments') {
                           $table->{$type}($column);
                       } else {
                           $table->{$type}($column)->nullable();
                       }
                   }
                });
            }

            foreach ($fields as $column => $type) {
                if (!C::schema()->hasColumn($table, $column)) {
                    C::schema()->table($table, function ($table) use ($type, $column) {
                        if ($type == 'increments') {
                            $table->{$type}($column);
                        } else {
                            $table->{$type}($column)->nullable();
                        }
                    });             
                }
                
            }
            
            $existingColumns = C::schema()->getColumnListing($table);
            $columns = array_keys($fields);
            $removeColumns = array_diff($existingColumns, $columns);
            foreach ($removeColumns as $column) {
                C::schema()->table($table, function ($table) use ($column) {
                   $table->dropColumn($column);
                });   
            }
            
        }
    }
}