<?php

/* Redbean */
use \RedBeanPHP\R as RedBeanPHP;

class R extends RedBeanPHP {}

/* Capsule */
use Illuminate\Database\Capsule\Manager as Capsule;
class C extends Capsule {}


class dbManager
{
    public $msName = null;

    public $client = null;

    public $db_name = null;

    public function __construct($msName)
    {
        $this->msName = $msName;
        $app = \Slim\Slim::getInstance();
        $auth = $app->auth;
        $this->client = $auth->getClientId();
        $this->db_name = $this->generateDbName($this->client);
    }

    public function __destruct()
    {
        R::close();
    }

    public function connect($dbHandler)
    {
        if (is_array($dbHandler)) {
            foreach ($dbHandler as $db) {
                $this->connectToDb($db);
            }

            return true;
        } elseif (is_string($dbHandler)) {
            $this->connectToDb($dbHandler);

            return true;
        }

        return false;
    }

    private function connectToDb($db)
    {
        switch ($db) {
            case 'redbean':
                try {
                    R::addDatabase($this->db_name, 'mysql:host=' . ApiConfig::get('dbHost') . ';dbname=' . $this->db_name, ApiConfig::get('dbUser'), ApiConfig::get('dbPassword'));
                } catch (Exception $e) {
                    //todo not sure actually what to do when error "A database has already be specified for this key" comes.:D
                }
                R::selectDatabase($this->db_name);

                R::exec('SET SESSION time_zone = "+0:00";');

                R::useWriterCache(false); // disable caching as it's conflicting with capsule

                /* Freeze DB structure if it's live environment */
                R::freeze(ApiConfig::isLive());

                break;
            case 'capsule':
            default:
                $capsule = new C;
                $capsule->addConnection([
                    'driver'    => 'mysql',
                    'host'      => ApiConfig::get('dbHost'),
                    'database'  => $this->db_name,
                    'username'  => ApiConfig::get('dbUser'),
                    'password'  => ApiConfig::get('dbPassword'),
                    'charset'   => 'utf8',
                    'collation' => 'utf8_unicode_ci',
                    'prefix'    => ApiConfig::get('capsule_prefix'),
                    'timezone'  => '+0:00'
                ], 'default');
                $capsule->setAsGlobal();
                $capsule->setFetchMode(PDO::FETCH_ASSOC);
                break;
        }
    }


    public function generateDbName($client = null)
    {
        $client = $client ? $client : $this->client;
        $db_name = $client . '_' . $this->msName;
        return $db_name;
    }
}

?>
