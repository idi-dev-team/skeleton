<?php

class Controller extends Application 
{
	
	public function __construct($app = null) 
	{
		parent::__construct($app);
		$this->Model = new Model($app);
	}

	public function renderData($data)
	{
		$return['data'] = $data;
		return $this->app->render(200, $return);
	}

	public function renderItems($items, $countAll, $start, $limit)
	{
		$data = array(
			'items'			=> $items,
			'pagination'	=> array(
				'items_count'	=> count($items),
				'total_items'	=> $countAll,
				'total_pages'	=> ceil($countAll / $limit),
				'current_page'	=> ceil($start / $limit) + 1,
				'start'			=> $start,
				'limit'			=> $limit,
			),
		);
		$this->renderData($data);
	}
	
	public function response($body)
	{
		$response = $this->app->response();
		$response['Content-Type'] = 'application/json';
		$response->body(json_encode(array($body)));
	}

	public function startConsumer()
	{
        $this->validate('auth.interApiCall.get'); // random permission name which will allow only inter api calls
        
        $data = $this->app->hooks->startConsumer();
        
        if (isset($data['error'])) {
            $this->throwError($data['msg'], 401);
        }
        
        $this->renderData($data);
	}

	public function consumeData()
	{
        if (ApiConfig::isLive()) {
            $this->throwError("Access Denied!");
        }
        $this->validate('auth.interApiCall.get'); // random permission name which will allow only inter api calls
        $this->app->hooks->consumeMessage($this->post());
        $this->renderData(true);
	}
	
    public function getSettings() 
    {
        return [];
    }
    
    public function updateSettings() {
        $settings = $this->getSettings();
        $app = \Slim\Slim::getInstance();
        $app->hooks->triggerEvent('updateSettings', $settings);
    }
    
    public function hookAuthRebuildSettings($data) 
    {
        return $this->updateSettings();
    }
}