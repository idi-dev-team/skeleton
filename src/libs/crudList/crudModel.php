<?php
/**
 *
 */

namespace crudList\Models;

use Illuminate\Database\Capsule\Manager as Capsule;

abstract class CrudModel
{
    public $id = null;

    // Extend your class by adding here extra arguments. All CRUD operations will work on them.

    public function __construct($args = array(), $extraParams = false)
    {
        if (is_array($args) || is_object($args)) {
            $this->setArgs($args, $extraParams);
        } else if (is_integer($args) && $args > 0) {
            $this->id = $args;
            $this->load();
        }
    }

    public function setArgs($args, $extraParams = false)
    {
        foreach ($args as $key => $value) {
            if (property_exists($this, $key)) {
                $this->$key = $value;
            } else if ($extraParams) {
                $this->$key = $value;
            }
        }
        return true;
    }


    public function load($keys = null)
    {
        $query = Capsule::table($this->getTable());

        if ($keys && is_array($keys)) {
            foreach ($keys as $key) {
                if (isset($key['field']) && isset($key['value'])) {
                    $query->where($key['field'], $key['value']);
                }
            }
        } else if ($this->id) {
            $query->where('id', $this->id);
        }

        $data = $query->first();

        if ($data){
            $this->setArgs($data);
        } else {
            $this->id = null;
        }
        return true;
    }

    /*
     *
     *  Merge object data set by setArgs() with data retrieved from DB.
     * 
     * */
    public function merge()
    {
        $query = Capsule::table($this->getTable());
        $query->where('id', $this->id);
        $data = $query->first();

        $attributes = get_object_vars($this);

        if ($data) {
            foreach ($attributes AS $name => $attribute)
            {
                if (is_null($this->$name)) {
                    $this->$name = $data[$name];
                }
            }
        }

        return ;
    }


    public function save($rewrite = false)
    {
        $validation = $this->validate();
        if  (!$validation['isValid']) {
            return $validation['error'];
        }

        $query = Capsule::table($this->getTable());

        $attributes = get_object_vars($this);
        unset($attributes['id']);

        $date = date("Y-m-d H:i:s"); //

        if (!isset($this->updated_at)) {
            $this->set('updated_at', $date);
        }
        // For Marius special
        if (!isset($this->modified_at)) {
            $this->set('modified_at', $date);
        }


        if (isset($this->id) && !empty($this->id)) {
            if ($rewrite) {
                $query->insert($attributes);
            } else {
                $query->where('id', $this->id);
                $query->update($attributes);
            }
        }else {
            if (!isset($this->created_at)) {
                $this->set('created_at', $date);
            }
            $this->id = $query->insertGetId($attributes);
        }
        return true;
    }

    public function validate()
    {
        $validation = array();

        /* VALIDATION CODE */
        /* If valid returns array 'isValid' = true */
        /* If invalid returns array 'isValid' = false, and 'error' = 'Error message' */

        $validation['isValid'] = true;

        return $validation;
    }

    public function validateEmail($field = 'email')
    {
        if (isset($this->$field) && !filter_var($this->$field, FILTER_VALIDATE_EMAIL)) {
            return false;
        }
        return true;
    }


    // list of filters : http://php.net/manual/en/filter.filters.validate.php
    // todo FILTER_VALIDATE_REGEXP enable in validation | ENABLED, need to be tested.
    public function validateField($field, $filter = null, $pattern = null)
    {
        if ( !isset($this->$field) || (isset($this->$field) && empty($this->$field)) ) {
            return false;
        }

        $options = null;
        if ($filter && $pattern) {
            $options = array("options" => array("regexp" => $pattern));
        }

        if ($filter && !filter_var($this->$field, $filter, $options)) {
            return false;
        }

        return true;
    }

    public function delete($destroy = false)
    {
        $query = Capsule::table($this->getTable());
        if ($this->id) {
            $query->where('id', $this->id);
            if (property_exists($this, 'deleted')) {
                if ($destroy == true) {
                    $query->delete();
                } else {
                    $query->update(['deleted' => 1]);
                    $this->deleted = 1;
                }
            } else {
                $query->delete();
            }
        }
        return true;
    }

    public function exist()
    {
        if ($this->id) {
            $query = Capsule::table($this->getTable());
            $query->select(["id"]);
            $query->where('id', $this->id);
            $data = $query->first();
            if ($data) {
                return true;
            } else {
                return false;
            }

        } else {
            return false;
        }
    }

    public function set($arg = null, $value = null)
    {
        if ($arg && property_exists($this, $arg)) {
            $this->$arg = $value;
            return true;
        }

        return false;

    }

    public function get($arg = null)
    {
        if ($arg && property_exists($this, $arg)) {
            return $this->$arg;
        } else {
            $attributes = get_object_vars($this);
            return $attributes;
        }
    }

    public function getById($id = null)
    {
        $this->id = $id;
        $this->load();
        if (empty($this->id)) return false;
        return $this->get();
    }

    public function getId()
    {
        return (int)$this->id;
    }

    public function setId($id = null)
    {
        $this->id = $id;
        return true;
    }

    /**
     *  Should return table name when extended by other class.
     */
    abstract public function getTable();

}


?>