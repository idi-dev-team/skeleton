<?php
/**
 *
 */

namespace crudList\Models;

use Illuminate\Database\Capsule\Manager as Capsule;


abstract class ListModel
{

    public $pagination = null;
    public $sort = null;
    public $filters = array();
    public $list = array();

    protected $crudModel = null;
    protected $export = false;


    public function __construct($start, $limit, $sort, $filters, $modelName = null)
    {
        if ($modelName) $this->setModel($modelName);
        $this->setArgs($start, $limit, $sort, $filters);
    }

    public function setArgs($start, $limit, $sort, $filters)
    {
        $start = $start ? $start : 0;
        $limit = $limit ? $limit : $this->getLimit();

        $this->pagination = array('start' => (int)$start, 'limit' => (int)$limit, 'currentPage' => null, 'totalItems' => null, 'totalPages' => null);

        $sort_pattern = "/^([+-]?)([a-zA-Z0-9_]+)$/i";
        if (preg_match($sort_pattern, $sort, $matches)) {
            $sortOrder = 'ASC';
            if ($matches[1] == '-') {
                $sortOrder = 'DESC';
            }
            $this->sort = array('sortBy' => $matches[2], 'sortOrder' => $sortOrder);
        } else {
            $this->sort = array('sortBy' => 'id', 'sortOrder' => 'DESC'); // default value
        }

        if ($filters && is_array($filters)) {
            foreach ($filters as $filter) {
                // Check if filter has or statement in it like: filter/foo=bar||bar=foo
                $generic = false; // generic means that filter has OR
                $genericFilters = array();
                if (strpos($filter, '||') !== false) {
                    $generic = true;
                    $subFilters = explode('||', $filter);
                } else {
                    $subFilters = array($filter);
                }

                foreach ($subFilters as $filter) {
                    if (strpos($filter, '==') !== false) {
                        $filter = explode('==', $filter);
                        $compare = 'LIKE';
                        $filter[1] = '%' . $filter[1] . '%';
                    } else if (strpos($filter, '!=') !== false) {
                        $filter = explode('!=', $filter);
                        $compare = '<>';
                    } else if (strpos($filter, '=IN=') !== false) {
                        $filter = explode('=IN=', $filter);
                        $compare = 'IN';
                        $filter[1] = explode(',', $filter[1]);
                    } else if (strpos($filter, '=NIN=') !== false) {
                        $filter = explode('=NIN=', $filter);
                        $compare = 'NIN';
                        $filter[1] = explode(',', $filter[1]);
                    } else if (strpos($filter, '=') !== false) {
                        $filter = explode('=', $filter);
                        $compare = '=';
                    } else if (strpos($filter, '>') !== false) {
                        $filter = explode('>', $filter);
                        $compare = '>';
                    } else if (strpos($filter, '<') !== false) {
                        $filter = explode('<', $filter);
                        $compare = '<';
                    } else {
                        continue;
                    }

                    if (isset($filter[0]) && isset($filter[1])) {
                        if ($filter[0] == 'search' && $compare == '=' && !$generic) {
                            $this->generateGenericFilters($filter[1]);
                        } else {
                            if (!$generic) {
                                $this->filters[] = array('filterBy' => $filter[0], 'filterValue' => $filter[1], 'compare' => $compare);
                            } else {
                                $genericFilters[] = array('filterBy' => $filter[0], 'filterValue' => $filter[1], 'compare' => $compare);
                            }

                        }
                    }
                }

                if ($generic && !empty($genericFilters)) {
                    $this->filters[] = array('type' => 'generic', 'filters' => $genericFilters);
                }
            }
        }

    }

    public function load($loadSingleObject = false)
    {
        $query = $this->loadInitialQuery();

        // $select = $this->getTable().'.*';
        $query->select('*');

        $query = $this->assignFilterToQuery($query);
        $query = $this->assignOrderToQuery($query);

        if (!$this->export) {
            $query->skip($this->getStart());
            $query->take($this->getLimit());
        }

        $data = $query->get();

        foreach ($data as $index => $row) {
            $obj = new $this->crudModel($row);
            if ($loadSingleObject) {
                $obj->load();
            }
            $this->list[$index] = $obj->get();
        }

        if (!$this->export) {
            $this->pagination['totalItems'] = $this->getTotalItems();
            $this->pagination['totalPages'] = $this->getTotalPages();
            $this->pagination['currentPage'] = $this->getCurrentPage();
        }

        return true;
    }

    // Batch delete
    public function delete()
    {
        $query = $this->loadInitialQuery();
        $query = $this->assignFilterToQuery($query);
        $query->delete();
    }

    protected function loadInitialQuery()
    {
        // TODO  $query = Capsule::table($this->getTable().' as MainTable'); this will allow to add join etc.
        $query = Capsule::table($this->getTable().'');

        if (property_exists($this->crudModel, 'deleted')) {
            $query->where('deleted', '=', '0');
        }

        //example
        if (property_exists($this->crudModel, 'status')) {
            // ToDo we can integrate some default behavior on some specific object fields
        }


        return $query;
    }
    /*

     *
     * */
    protected function assignFilterToQuery(\Illuminate\Database\Query\Builder $query)
    {
        // ToDo finish this part
        foreach ($this->filters AS $filter) {
            if (isset($filter['type']) && $filter['type'] == 'generic') {
                // GENERIC filters
                $gQuery = $query->newQuery();
                foreach ($filter['filters'] as $genericFilter)
                {
                    $this->assignSingleFilterToQuery($genericFilter, $gQuery, true);
                    // $gQuery->orWhere($genericFilter['filterBy'], $genericFilter['compare'], $genericFilter['filterValue']);
                }
                $query->addNestedWhereQuery($gQuery);
            } else {
                if ($filter['filterBy'] == 'export') {
                    $this->setExport(true);
                    continue;
                }
                $this->assignSingleFilterToQuery($filter, $query);
            }
        }
        // echo $query->toSql();die();
        return $query;
    }

    protected function assignSingleFilterToQuery($filter, \Illuminate\Database\Query\Builder $query, $orWhere = false)
    {
        if (property_exists($this->crudModel, $filter['filterBy'])) {
            if ($filter['compare'] == 'IN') {
                if ($orWhere) {
                    $query->orWhereIn($filter['filterBy'], $filter['filterValue']);
                } else {
                    $query->whereIn($filter['filterBy'], $filter['filterValue']);
                }

            } else if ($filter['compare'] == 'NIN') {
                if ($orWhere) {
                    $query->orWhereNotIn($filter['filterBy'], $filter['filterValue']);
                } else {
                    $query->whereNotIn($filter['filterBy'], $filter['filterValue']);
                }
            } else {
                if ( ($filter['compare'] == '=' || $filter['compare'] == '<>') && ( strtolower($filter['filterValue']) == 'null')) {

                    switch ($filter['compare']) {
                        case '=':
                            if ($orWhere) {
                                $query->orWhereNull($filter['filterBy']);
                            } else {
                                $query->whereNull($filter['filterBy']);
                            }
                            break;
                        case '<>':
                            if ($orWhere) {
                                $query->orWhereNotNull($filter['filterBy']);
                            } else {
                                $query->whereNotNull($filter['filterBy']);
                            }
                            break;
                    }
                } else {
                    if ($orWhere) {
                        $query->orWhere($filter['filterBy'], $filter['compare'], $filter['filterValue']);
                    } else {
                        $query->where($filter['filterBy'], $filter['compare'], $filter['filterValue']);
                    }

                }
            }
        } else {
            // in extended class you can overwrite this by adding joining tables etc.
        }

        return;
    }

    /*
     * $searchQuery is list of values that should match single objected separate with ++
     * Example: $searchQuery = John++2015-02-15 ,
     *
    */
    protected function generateGenericFilters($searchQuery)
    {
        $searchItems = explode('++', $searchQuery);
        $searchFields = $this->getModelAttributes();
        $filters = array();
        if ($searchFields) {
            foreach ($searchFields as $field => $fieldValue) {
                foreach ($searchItems as $item) {
                    $item = '%'.$item.'%';
                    $filters[] = array('filterBy' => $field, 'filterValue' => $item, 'compare' => 'LIKE');
                }
            }
            if ($filters) {
                $this->filters[] = array('type' => 'generic', 'filters' => $filters);
            }
        }

        return;
    }


    protected function assignOrderToQuery(\Illuminate\Database\Query\Builder $query)
    {
        $query->orderBy($this->sort['sortBy'], $this->sort['sortOrder']);
        return $query;
    }

    public function setExport($value)
    {
        $this->export = $value;
        return true;
    }


    public function get()
    {
        $data['list'] = $this->list;
        if (!$this->export) {
            $data['pagination'] = $this->pagination;
        }
        return $data;
    }

    public function getTotalItems()
    {
        $query = $this->loadInitialQuery();
        $query->select( $query->raw('COUNT(id) AS total'));
        $query = $this->assignFilterToQuery($query);
        $total = (array)$query->first();
        return $total['total'];

    }

    public function getStart()
    {
        return $this->pagination['start'];
    }

    public function setLimit($limit = 25)
    {
        if ($limit > 0) {
            $this->pagination['limit'] = $limit;
        }
        return $this->pagination['limit'];
    }

    public function getLimit()
    {
        if (isset($this->pagination['limit'])) {
            return $this->pagination['limit'];
        } else {
            return 25;
        }
    }

    public function getTotalPages()
    {
        $total = $this->getTotalItems();
        $limit = $this->getLimit();
        if ($total != 0) {
            if ($total % $limit == 0) {
                $this->pagination['totalPages'] = (int)($total / $limit);
            } else {
                $this->pagination['totalPages'] = (int)($total / $limit + 1);
            }
        } else {
            $this->pagination['totalPages'] = 0;
        }
        return $this->pagination['totalPages'];
    }

    public function getCurrentPage()
    {
        $start = $this->getStart();
        $limit = $this->getLimit();
        if ($start % $limit == 0) {
            $currentPage = (int)($start / $limit) + 1;
        } else {
            $currentPage = (int)($start / $limit + 1) + 1;
        }
        return $currentPage;
    }


    public function setModel($modelName, $namespace = '\auth\Models\\')
    {
        $class = $namespace.$modelName;
        $this->crudModel = $class;
        return true;
    }

    public function getModel()
    {
        return $this->crudModel;
    }

    public function getModelAttributes()
    {
        if ($this->crudModel) {
            $attributes = get_class_vars($this->crudModel);
            return $attributes;
        }
        return false;
    }

    public function getTable()
    {
        $obj = new $this->crudModel();
        return $obj->getTable();
    }

}

?>