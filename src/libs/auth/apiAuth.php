<?php 
/**
 *  API Communication and authentication class
 *  For USE in internal inter micro service communications
 *  Creates and Authenticates hashed signatures - based on requests and sends to api via curl
 */


class ApiAuth
{
    /**
     * Construct, Sign and Send API POST call
     * @param  url of api call
     * @param  array of post parameters
     * @return string request response
     */
    public static function sendPostRequest($url, $method, $postargs, $inter_token, $requestType = 'json')
    {

        $header = [
            'Authorization: Inner ' . $inter_token
        ];

        $curl_handle = curl_init();

        if ($requestType == 'json') {
            $postargs = json_encode($postargs);
            $header[] = 'Content-Type: application/json';
            $header[] = 'Content-Length: ' . strlen($postargs);
        }

        curl_setopt($curl_handle, CURLOPT_URL, $url);
        if ($method == 'POST') {
            curl_setopt($curl_handle, CURLOPT_POST, true);
        } else {
            curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, $method);
        }

        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl_handle, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $postargs);
        curl_setopt($curl_handle, CURLOPT_USERAGENT, 'MS-' . ApiConfig::get('msName'));
        curl_setopt($curl_handle, CURLOPT_TIMEOUT, 60);
        curl_setopt($curl_handle, CURLOPT_HTTPHEADER, $header);

        $response = curl_exec($curl_handle);
            
        if (curl_error($curl_handle)) {
            $app = Slim\Slim::getInstance();
            $app->log->error('Curl error: {error}. URL: {url} Method: {method}', array(
                'error' =>  curl_error($curl_handle),
                'url' => $url,
                'method' => $method
            ));
        }
        curl_close($curl_handle);

        return $response;
    }

    /**
     *
     * Construct, Sign and Send API GET call
     * @param  url of api call
     * @return string request response
     *
     */
    public static function sendGetRequest($url, $inter_token)
    {
        $header = [
            'Authorization: Inner ' . $inter_token
        ];
        $curl_handle = curl_init();
        curl_setopt($curl_handle, CURLOPT_URL, $url);
        curl_setopt($curl_handle, CURLOPT_POST, false);
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl_handle, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl_handle, CURLOPT_USERAGENT, 'MS-' . ApiConfig::get('msName'));
        curl_setopt($curl_handle, CURLOPT_TIMEOUT, 60);
        curl_setopt($curl_handle, CURLOPT_HTTPHEADER, $header);

        $response = curl_exec($curl_handle);

        if (curl_error($curl_handle)) {
            $app = Slim\Slim::getInstance();
            $app->log->error('Curl error: {error}. URL: {url} Method: {method}', array(
                'error' =>  curl_error($curl_handle),
                'url' => $url,
                'method' => 'GET'
            ));
        }
        
        curl_close($curl_handle);

        return $response;
    }
}
