<?php

namespace api;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Exception\AMQPTimeoutException;
use PhpAmqpLib\Message\AMQPMessage;

class Hooks {
    
    private $host;
    
    private $port;
    
    private $user;
    
    private $password;
    
    private $exchange;
    
    private $consumeLimit;

    public function __construct() 
    {
        $this->host         = \ApiConfig::get('rabbitHost', 'localhost');
        $this->port         = \ApiConfig::get('rabbitPort', 5672);
        $this->user         = \ApiConfig::get('rabbitUser', 'guest');
        $this->password     = \ApiConfig::get('rabbitPassword', 'guest');
        $this->consumeLimit = \ApiConfig::get('rabbitConsumeLimit', 60);
        $this->exchange     = \ApiConfig::get('rabbitExchange', 'hooks');
    }
    
    public function startConsumer()
    {
        if (!class_exists('BaseController'))  {
            return ['error' => 1, 'msg' => 'BaseController missing!'];
        }
        
        $app = \Slim\Slim::getInstance();
        
        $connection = new AMQPStreamConnection($this->host, $this->port, $this->user, $this->password, $app->auth->getClientId());
        $channel = $connection->channel();
        $channel->exchange_declare($this->exchange, 'fanout', false, true, false);
        
        $queueName = 'queue-' . \ApiConfig::get('msName');
        $channel->queue_declare($queueName, false, true, false, false);
        $channel->queue_bind($queueName, $this->exchange);
        
        $callback = function($message) use ($app) {
            $json = json_decode($message->body, true);
            try {
                $this->consumeMessage($json);
            } catch (\Exception $ex) {
                $app->log->error($ex->getMessage() . ': ' . $message->body);
            }
            $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
        };
        
        $channel->basic_consume($queueName, '', false, false, false, false, $callback);
        
        $start = time();
        while(count($channel->callbacks) && (time() - $start) < $this->consumeLimit) {
            try {
                $channel->wait(null, false, $this->consumeLimit);
            } catch (AMQPTimeoutException $exception) {
            }
        }
        
        $channel->close();
        $connection->close();
        
        return true;
	}

	public function consumeMessage($json)
	{
		$app = \Slim\Slim::getInstance();

		$controller = new \BaseController($app);
		if (!method_exists($controller, 'hook' . ucfirst($json['ms']) . ucfirst($json['name']))) {
			return;
		}

		$app->token = null;

		// Set token in headers for Auth
		$app->request()->headers->set('authorization', 'Bearer ' . $json['token']);

		// Authenticate - get access data (permissions, user and client)
		$app->container->singleton('auth', function () {
			return new \api\Auth\Authentication();
		});
		$app->auth->init();

		// Check is we have access
		if (!$app->auth->hasAccess()) {
			$app->log->error('Can\'t consume message: {message}', array(
				'message' =>  json_encode($json)
			));
			return false;
		}

		$controller->{'hook' . ucfirst($json['ms']) . ucfirst($json['name'])}($json['data']);
	}
        
	public function triggerEvent($name, $data)
	{
        if (\ApiConfig::get('disableHooks')) {
            return false;
        }
  
        $client = \Slim\Slim::getInstance()->auth->getClientId();
        
        try {
            $connection = new AMQPStreamConnection($this->host, $this->port, $this->user, $this->password, $client);
        } finally {
            if (!isset($connection)) {
                // TODO: log lost data
                return false;
            }
        }
        
        $channel = $connection->channel();
        $channel->exchange_declare($this->exchange, 'fanout', false, true, false);
        
        $json = json_encode([
            'token' => \Slim\Slim::getInstance()->auth->token,
            'name' => $name,
            'ms' => \ApiConfig::get('msName'),
            'data' => $data            
        ]);
        
        $msg = new AMQPMessage($json, array('delivery_mode' => 2));
        $channel->basic_publish($msg, $this->exchange);
        
        $channel->close();
        $connection->close();
        
        return true;
    }

}