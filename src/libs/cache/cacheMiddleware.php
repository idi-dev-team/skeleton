<?php

namespace libs\cache;

class CacheMiddleware extends \Slim\Middleware {

    public function call() {

        $key_name = $this->app->request()->getResourceUri();

        $cacheAPI = \ApiConfig::get('cacheAPI');
        if ($cacheAPI && $this->app->request->isGet()) {

            $settings = \ApiConfig::get('cacheSettings');
            $cache = new Cache($settings['store'], $settings['prefix']);

            $rsp = $this->app->response();

            // Check cache
            if ($cache->exist($key_name)) {

                // Return content from cache
                $data = $cache->fetch($key_name);
                foreach ($data['header'] as $key => $value) {
                    $rsp->headers->set($key, $value);
                }
                $rsp->body($data["body"]);
                return;

            }

            // Not in cache. Call controller
            $this->next->call();

            // Cache the content
            if (($rsp->status() == 200)) {
                $header = $rsp->headers->all();
                $data = array(
                    'header' => $header,
                    'body' => $rsp->body()
                );
                $cache->store($key_name, $data, 600);
            }

        } else {
            $this->next->call();
        }

    }
}