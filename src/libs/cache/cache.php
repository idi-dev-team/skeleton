<?php

namespace libs\cache;

class Cache
{
    public $storeEngine = 'files';

    public $prefix = 'cache_';

    public $cache = null;

    public function __construct($storeEngine = null, $prefix = 'cache_')
    {

        $this->storeEngine = $storeEngine?$storeEngine:'files';
        $this->suffix = $prefix?$prefix:'cache_';

        // setup your cache
        $config = array(
            "storage"   =>  $this->storeEngine,
            "default_chmod" => 0644,
            "htaccess"      => true,

            // path to cache folder, leave it blank for auto detect
            "path"      =>  PROJECT_ROOT."/cache",
            "securityKey"   =>  "auto", // auto will use domain name, set it to 1 string if you use alias domain name

            /*
             * Fall back when old driver is not support
             */
            "fallback"  => "files",
        );

        $this->cache = new \phpFastCache($storeEngine, $config);

    }

    public function exist($key)
    {
        $keyword = $this->generateUniqueKey($key);
        return $this->cache->isExisting($keyword);
    }

    public function store($key, $data, $timeout = 300)
    {
        $keyword = $this->generateUniqueKey($key);
        $this->cache->set($keyword, $data, $timeout);
        return true;
    }

    public function fetch($key)
    {
        $keyword = $this->generateUniqueKey($key);
        $data = $this->cache->get($keyword);
        return $data;
    }

    public function destroy($key)
    {
        $keyword = $this->generateUniqueKey($key);
        $this->cache->delete($keyword);
        return true;
    }


    private function generateUniqueKey($key)
    {
        $key = $this->suffix.'_'.$key;
        return hash('sha256', $key);
    }

}