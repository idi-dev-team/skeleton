<?php

abstract class SkeletonConfig 
{
	static public $isInit;
	static public $env;
	static public $dbHost;
	static public $dbUser;
	static public $dbPassword;
	static public $msName;
	static public $liveUrl;
	static public $devUrl;
	static public $localUrl;
	static public $secret;
	static public $dbHandler = array();
	static public $libs = array();
	static public $unsecuredEnv = array();
	static public $capsule_prefix = '';
	static public $disableHooks = false;
    
	static public $cacheAPI = false; // true or false
	static public $cacheCalls = false; // true or false
	static public $cacheSettings;
    
	static public $rabbitHost = 'localhost';
	static public $rabbitPort = 5672;
	static public $rabbitUser = 'guest';
	static public $rabbitPassword = 'guest';
	static public $rabbitExchange = 'hooks';
	static public $rabbitConsumeLimit = 60; // seconds

	public static function get($param, $default = null) 
	{
        $return = (property_exists('ApiConfig', $param)) ? ApiConfig::$$param : $default;
		return $return;
	}

    public static function getApiUrl($apiName)
    {
        $env = ApiConfig::get('env');
        $envUrl = $env . 'Url';
        $url = ApiConfig::$$envUrl . '/' . $apiName . '/v1';

        // Check if path override was defined in config by ApiName
        $customPath = $apiName . 'CustomPath';
        if (isset(ApiConfig::$$customPath)) {
            $url = ApiConfig::$$envUrl . '/' . ApiConfig::$$customPath;
        }

        return $url;
    }

	public static function loadLibs() 
	{
		$libs = ApiConfig::get('libs', array());
		foreach ($libs as $class => $path) {
			require_once getcwd() . "/app/libs/$path/$class.php";
		}
	}

	public static function isLive() 
	{
		return ApiConfig::get('env') == 'live';
	}

	public static function isDev() 
	{
		return ApiConfig::get('env') == 'dev';
	}

	public static function isLocal() 
	{
		return ApiConfig::get('env') == 'local';
	}
}