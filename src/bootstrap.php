<?php

// Settings to make all errors more obvious during testing
error_reporting(-1);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
date_default_timezone_set('UTC');

// Fix notices because we are not using Slim from Browser context
$_SERVER['REQUEST_URI'] = $_SERVER['REMOTE_ADDR'] = $_SERVER['SERVER_NAME'] = $_SERVER['REQUEST_METHOD'] = '';

use There4\Slim\Test\WebTestCase;

define('PROJECT_ROOT', realpath(__DIR__ . '/../../../..'));
define('UNIT_TESTING', 1);

/* Load Composer Autoload */
require_once PROJECT_ROOT . '/vendor/autoload.php';


/* Load Controllers */
require_once PROJECT_ROOT . "/vendor/idiapi/skeleton/src/config.php";
require_once PROJECT_ROOT . "/app/config.php";
require_once PROJECT_ROOT . "/vendor/idiapi/skeleton/src/authentication.php";
require_once PROJECT_ROOT . "/vendor/idiapi/skeleton/src/dbManager.php";
require_once PROJECT_ROOT . "/vendor/idiapi/skeleton/src/application.php";
require_once PROJECT_ROOT . "/vendor/idiapi/skeleton/src/model.php";
require_once PROJECT_ROOT . "/vendor/idiapi/skeleton/src/controller.php";
foreach (glob(PROJECT_ROOT . "/app/controllers/*.php") as $controller) {
    require_once $controller;
}

/* Load General Libs */
require_once "libs/auth/apiAuth.php";

/* Load cache class and cacheMiddleware */
require_once "libs/cache/cache.php";
require_once "libs/cache/cacheMiddleware.php";

/* Load Libs */
ApiConfig::loadLibs();

// Initialize our own copy of the slim application
class LocalWebTestCase extends WebTestCase {
	public function __construct() {
		parent::__construct();
        
        $app = new \Slim\Slim(array('debug' => true));

        /* Add containers to Slim */
        // Hooks
        $app->container->singleton('hooks', function () {
            return new api\Hooks();
        });
        
        $app->token = '';
        // Authenticate - get access data (permissions, user and client)
        $app->container->singleton('auth', function () {
            return new api\Auth\Authentication();
        });
        $app->auth->init();
        
        // Connect to Database
        $dbManager = new dbManager(ApiConfig::get('msName'));
        if (!$dbManager->connect(ApiConfig::get('dbHandler'))) {
            $this->throwError('Connection with database interrupted');
        }
	
		/* JSON Api View and Middleware */
		$app->view(new \JsonApiView());
		$app->add(new \JsonApiMiddleware());
		$this->controller = new Controller($app);
	}
	
	public function getSlimInstance() {
		$app = new \Slim\Slim(array(
				'version'        => '0.0.0',
				'debug'          => false,
				'mode'           => 'testing',
		));

		return $app;
	}
	
	public function postSampleFile($type = 'profileimage', $entity_id = '123456789') 
	{
		// Create File
		$filename = 'attachment_' . md5(microtime()) . '.txt';
		$file = getcwd() . '/tests/' . $filename ;
		file_put_contents($file, 'test');
		
		$url = ApiConfig::getApiUrl('filemanager') . '/file/upload/' . $type . '/' . $entity_id;
		$sig = ApiAuth::signRequest($url, null);
        $url = $url . "?access_token=&inter_token=".$sig;
		
		$params = array(
			'file' => new CurlFile($file, 'text/plain', $filename)
		);
		
		$ch = curl_init($url); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $params); 
		$response = curl_exec($ch);       
		
		unlink($file);
		
		$data = json_decode($response)->data;
		
		return $data;
	}
		
	public function setUp()
	{ 
		parent::setUp();
	}
	
	public function tearDown()
	{
		parent::tearDown();
	}
}

