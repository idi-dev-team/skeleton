<?php

namespace api\Auth;


use Idi\ApiAuthMS\Exception\NoTokenException;
use Idi\ApiAuthMS\Service\TokenValidator;
use Idi\Slim3General\Exception\AccessDeniedException;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Key;
use Lcobucci\JWT\ValidationData;

class Authentication
{
    const TYPE_BEARER = 'Bearer';
    const TYPE_INNER = 'Inner';

    static $availableTypes = [
        self::TYPE_BEARER,
        self::TYPE_INNER
    ];

    public $token = null;

    public $user = null;

    public $client = null;

    public $permissions = array();

    public $interApiCall = false;

    public function init()
    {

        $app = \Slim\Slim::getInstance();

        if ($app->request()->getMethod() === 'OPTIONS') {
            return true;
        }
        
        // Check if we have an valid token as Inter API Call

        $authorizationHeader = $app->request()->headers('authorization');

        if (!empty($authorizationHeader)) {
            $tokenParts = $this->extractToken($authorizationHeader);
            $this->token = $tokenParts['jwt'];

            if ($tokenParts['type'] == 'Inner') {
                $this->interApiCall = true;
            }
        }

        if ($this->token) {
            $this->loadUserAndClient();
            $this->loadPermissions();
        }
        
        if (!$this->user) {
            $this->user = (object) array(
                'id' => 0
            );
        }

        // Access always available when environment is in unsercureEnv list.
        if (in_array(\ApiConfig::get('env'), \ApiConfig::get('unsecuredEnv'))) {
            $this->interApiCall = true;
        }

    }

    public function hasAccess()
    {
        // if inter api call or is logged user
        if ($this->interApiCall || $this->getId()) {
            return true;
        } else {
            return false;
        }
    }

    private function loadPermissions()
    {
        // Get Permission for the token
        $permissions = \Application::call('GET', 'auth', 'users/' . $this->user->id . '/permission/all');

        if (isset($permissions->permissions)) {
            $this->permissions = $permissions->permissions;
        }

    }

    private function loadUserAndClient()
    {
        $app = \Slim\Slim::getInstance();
        $settings = \ApiConfig::get('authKey');

        $validator = new TokenValidator(
            new Parser(),
            new ValidationData(),
            new Key(
                'file://' . PROJECT_ROOT . $settings['public'],
                $settings['password']
            )
        );

        try {
            $jwt = $validator->validate($this->token);
        } catch (\Exception $exception) {
            throw new AccessDeniedException('Invalid Token');
        }

        $app->token = $jwt;

        $userId =  $jwt->getClaim('sub');

        // Setup user and client
        $this->user  = \Application::call('GET', 'auth', 'users/' . $userId);
        $this->client = (object) [
            'id' => $this->user->tenant
        ];

    }

    public function getInstance()
    {
        return $this;
    }

    public function getId()
    {
        return $this->getUserId();
    }

    public function getUserId()
    {
        return (isset($this->user->id) ? $this->user->id : false);
    }

    public function getClientId()
    {
        return (isset($this->client->id) ? $this->client->id : 'default');
    }

    public function getToken()
    {
        return $this->token;
    }

    public function getPermissions()
    {
        return $this->permissions;
    }

    // Checks if user has permission or permissions
    // Possible value for $permissions
    // $permissions = array('permission1', 'permission2');
    // $permissions = 'permission1, permission2';
    // $permissions = 'permission1';
    public function hasPermission($permissions, $allowInterApiCalls = true)
    {
        if ($allowInterApiCalls && $this->interApiCall) {
            return true;
        } else if (is_array($permissions)) {
            foreach ($permissions AS $permission) {
                if ($this->permissionExist($permission)) {
                    return true;
                }
            }
        } else {
            return $this->permissionExist($permissions);
        }
        return false;
    }
    

    private function permissionExist($permission)
    {
        $permissions = explode(',', $permission); // Check if permission is separated with comma
        if ($permissions) {
            foreach ($permissions as $singlePermission) {
                $singlePermission = trim($singlePermission);
                if (in_array($singlePermission, $this->permissions)) {
                    return true;
                }
            }
        }
        return false;
    }

    private function extractToken($authorizationHeader)
    {
        $parts = array_values(
            array_filter(
                explode(
                    ' ',
                    trim($authorizationHeader)
                )
            )
        );

        if (count($parts) !== 2) {
            throw new NoTokenException('Invalid token format');
        }

        if (!in_array($parts[0], self::$availableTypes)) {
            throw new NoTokenException('Unknown type of Authorization header');
        }

        return [
            "type" => $parts[0],
            "jwt" => $parts[1]
        ];
    }


}
