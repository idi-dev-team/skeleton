<?php

/* Load config */

define('PROJECT_ROOT', realpath(__DIR__ . '/../../../..'));
require_once PROJECT_ROOT . "/vendor/idiapi/skeleton/src/config.php";
require_once PROJECT_ROOT . "/app/config.php";

/* Load General Libs */

require_once PROJECT_ROOT . "/vendor/idiapi/skeleton/src/libs/auth/apiAuth.php";
require_once PROJECT_ROOT . "/vendor/idiapi/skeleton/src/libs/crudList/autoload.php";
require_once PROJECT_ROOT . "/vendor/idiapi/skeleton/src/libs/hooks/hooks.php";

/* Load cache class and cacheMiddleware */
require_once PROJECT_ROOT . "/vendor/idiapi/skeleton/src/libs/cache/cache.php";
require_once PROJECT_ROOT . "/vendor/idiapi/skeleton/src/libs/cache/cacheMiddleware.php";

/* Load Controllers */

require_once PROJECT_ROOT . "/vendor/idiapi/skeleton/src/authentication.php";
require_once PROJECT_ROOT . "/vendor/idiapi/skeleton/src/dbManager.php";
require_once PROJECT_ROOT . "/vendor/idiapi/skeleton/src/application.php";
require_once PROJECT_ROOT . "/vendor/idiapi/skeleton/src/model.php";
require_once PROJECT_ROOT . "/vendor/idiapi/skeleton/src/controller.php";


foreach (glob(PROJECT_ROOT . "/app/controllers/*.php") as $controller) {
    require_once $controller;
}

/* Load Models */
foreach (glob(PROJECT_ROOT . "/app/models/*.php") as $model) {
    require_once $model;
}

/* Load Services */
foreach (glob(PROJECT_ROOT . "/app/services/*.php") as $service) {
    require_once $service;
}

/* Load Custom MS Libs */
ApiConfig::loadLibs();

$logger = new \Flynsarmy\SlimMonolog\Log\MonologWriter(
    array(
        'name' => 'idi-api-' . ApiConfig::get('msName'),
        'handlers' => array(
            new \Monolog\Handler\StreamHandler('../logs/' . date('Y-m-d') . '.log'),
        ),
    )
);

/* Add containers to Slim */
$app = new \Slim\Slim([
    'log.writer' => $logger,
    'log.enabled' => true,
]);

// Hooks
$app->container->singleton('hooks', function () {
    return new api\Hooks();
});

// Authenticate - get access data (permissions, user and client)
$app->container->singleton('auth', function () {
    return new api\Auth\Authentication();
});
$app->auth->init();

/* SET PHP TIMEZONE */
date_default_timezone_set("UTC");

// Connect to Database
$dbManager = new dbManager(ApiConfig::get('msName'));
if (!$dbManager->connect(ApiConfig::get('dbHandler'))) {
    $this->throwError('Connection with database interrupted');
}


/* JSON Api View and Middleware */
$app->view(new \JsonApiView());
$app->add(new \JsonApiMiddleware());

/* Load cache Middleware */

if (ApiConfig::get('cacheAPI')) {  // If cache enabled
    $app->add(new \libs\cache\CacheMiddleware());
}

/* Add Custom Controllers Singletons */
foreach (glob(PROJECT_ROOT . "/app/controllers/*.php") as $controller) {
    $controllerName = ucfirst(basename($controller, '.php')) . 'Controller';
    $app->container->singleton($controllerName, function () use ($controllerName) {
        return new $controllerName();
    });
}

/* Routes */
$routesDir = new DirectoryIterator(PROJECT_ROOT . "/app/routes");

foreach (new IteratorIterator($routesDir) as $file) {
    $filename = $file->getFilename();

    if (substr($filename, -4) === ".php") {
        require_once($file->getPathname());
    }
}

/* Execute App */
$app->run();
