<?php

use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Key;
use Lcobucci\JWT\Signer\Rsa\Sha256;
use RandomLib\Factory;
use SecurityLib\Strength;


class Application
{
	
	public function __construct(\Slim\Slim $slim = null)
	{
		$app = !empty($slim) ? $slim : \Slim\Slim::getInstance();

		// Custom Error Handling
		$app->error(function (Exception $e) use ($app) {
			
			$code = ($e->getCode() === 0) ? "500" : $e->getCode();
			$app->response->setStatus($code);

			$app->response->headers->set('Content-Type', 'application/json');

			if ($code > 10 && !ApiConfig::isLocal()) {
			   $message = $e->getMessage();
			} else {
			   $message = $e->getMessage() . ' in ' . $e->getFile() . ' on line ' . $e->getLine() . nl2br($e->getTraceAsString());
			}
			
			echo json_encode([
			   "error" => true,
			   "msg" => $message,
			   "status" => $code,
			]);
		});

		$this->app = $app;
		
	}
	
	public function run()
	{
		$this->app->run();
	}

	public function redirect($name, $routeName = true)
	{
		$url = $routeName ? $this->app->urlFor($name) : $name;
		$this->app->redirect($url);
	}

	public function get($value = null)
	{
		return $this->app->request()->get($value);
	}
	
	public function getInt($value) 
	{
		return (int)$this->post($value);
	}

	public function post($value = null)
	{
		$data = json_decode($this->app->request()->getBody(), true);
		if ($value) {
			if (isset($data[$value])) {
				return $data[$value];
			} else {
				return null;
			}
		} 
		
		return $data;
	}

	public function throwError($error, $code = '500')
	{
		throw new Exception($error, $code);
	}

	public static function call($method, $apiName, $route,  $params = array())
	{
		$app = \Slim\Slim::getInstance();
        $settings = \ApiConfig::get('authKey');

        $accessToken = $app->token;
        $privateKey = new Key(
            'file://' . PROJECT_ROOT . $settings['private'],
            $settings['password']
        );
        $expiryDateTime = (new \DateTime('+1 minute'));
        $factory = new Factory();
        $randomGenerator = $factory->getGenerator(new Strength(Strength::MEDIUM));


		$url = ApiConfig::getApiUrl($apiName) . '/' . $route;

		$cacheCalls = ApiConfig::get('cacheCalls');
		$settings = \ApiConfig::get('cacheSettings');
		$cache = new \libs\cache\Cache($settings['store'], $settings['prefix']);
		$key = $method.$apiName.$route.serialize($params).$app->auth->token;
		if ($cacheCalls && $method == 'GET') {
			// Check cache
			if ($cache->exist($key)) {
				// Return content from cache
				$data = $cache->fetch($key);
				return $data;
			}
		}

        $builder = new Builder();


        $inter_token = $builder
            ->setAudience($accessToken->getClaim('aud'))
            ->setId($accessToken->getClaim('jti'), true)
            ->setIssuedAt(time())
            ->setNotBefore(time())
            ->setExpiration($expiryDateTime->getTimestamp())
            ->setSubject($accessToken->getClaim('sub'))
            ->set('inn', $randomGenerator->generateString(8))
            ->sign(
                new Sha256(),
                $privateKey
            )
            ->getToken()
        ;

		if ($method == 'GET') {
			$response = ApiAuth::sendGetRequest($url, $inter_token);
		} else {
			$response = ApiAuth::sendPostRequest($url, $method, $params, $inter_token);
		}

		$data = json_decode($response);
		if ($data === null) {
			return 'Empty Response';
		}
		if ($data->error) {
			return $data->msg;
		}

		if ($cacheCalls && $method == 'GET') {
			$cache->store($key, $data->data, $settings['timeout']);
		}

		return $data->data;

	}

	public function validate($permission = null, $kill = true)
	{
        $auth = $this->app->auth;

        if (!$permission) {
            $return = $auth->hasAccess();
        } else if ($auth->hasPermission($permission)) {
            $return = true;
        } else {
            $return = false;
        }
		
		if ($kill && !$return) {
			$this->throwError("Access denied!", 403);
		}

        return $return;

	}	
}